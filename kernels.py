import numpy
import param as par
import fclaux as fc
import scipy.integrate as scint
from scipy.interpolate import InterpolatedUnivariateSpline as interpolate
from scipy.integrate import trapz, simps
from itertools import product
import parmap
import tqdm

'''S kernels'''
def S0( r, x):
    y = 1 + r**2 - 2*r*x
    return r**2.*(3.*x**2-1.)/4.*(3.*x**2-2+3.*(r*x-1)**2/y)

def S1( r, x):
    y = 1 + r**2 - 2*r*x
    return 2.*r**2.*x*(1-x**2.)*(2.*r*x**2-r-x)/y

def S2( r, x):
    y = 1 + r**2 - 2*r*x
    return r**2.*(1.-x**2.)**2/8.*(1.+2*r**2-2.*r*x)/y
#F2    
def S3( r, x):
    y = 1 + r**2 - 2*r*x
    return r*(3.*x**2-1.)*(7.*x+3.*r-10.*r*x**2)/28./y

dictS = {1:S0, 2:S1, 3:S2, 4:S3}


#####Define double integral here
def S_internal(k, n, r):
    func = lambda r, x: ilpk(k*numpy.sqrt(1 + r**2 - 2*r*x))*dictS[n](r, x)
    return (glwval*func(r, glxval)).sum(axis = -1)

def S_external(n, k):
    fac = k**3 /(2*numpy.pi)**2	
    r = (kint/k)
    absr1 = abs(r-1)
    mask = absr1 < tol
    y = numpy.zeros_like(r)
    y[~mask]  = S_internal(k, n, r[~mask].reshape(-1, 1))
    if mask.sum():
        y[mask] = S_internal(k, n,  r[mask].reshape(-1, 1))  #as it is, I'm not masking anything
    y *= pkint
    return fac*trapz(y, r)

def S(kv, pk, npool = None, ns = None, kintv = None):

    global ilpk, tol, kint, pkint, glxval, glwval

    ilpk = pk
    if kintv is None:
        kint = numpy.logspace(-5, 3, 2e3)
    else:
        kint = kintv
    pkint = ilpk(kint)

    tol = 1e-5
    glxval, glwval = numpy.array(gl_128_X), numpy.array(gl_128_W)
    glxval = glxval.reshape(1, -1)
    glwval = glwval.reshape(1, -1)

    if ns is None:
        ns = [1, 2, 3, 4]

    prod = product(ns, kv)
    Sv = parmap.starmap(S_external, list(prod), pm_processes=npool, pm_chunksize=1, pm_pbar=True)
    Sv = numpy.array(Sv).reshape(len(ns), -1)
    toret = numpy.zeros([Sv.shape[0] + 1, Sv.shape[1]])
    toret[0] = kv
    toret[1:, :] = Sv

    del ilpk, tol, kint, pkint, glxval, glwval

    return toret

'''R kernel'''

def R0( r):
    val = 1./112.*(15.+73.*r**2-55.*r**4+15.*r**6)
    val += 15.*(r**2-1)**4/448./r*numpy.log((1.-r)**2/(1.+r)**2)
    return val

def R_external( k):
    fac = k**3 /(2*numpy.pi)**2	
    fee1 = lambda r : R0(r)*ilpk(r*k)
    val1 = scint.quad(fee1,qmin/k,tol,epsabs=0.,epsrel=1e-7, limit=100000000)
    fee2 = lambda r : (24./49.-8./147./r**2-8./1617/r**4-8./7007./r**6-8./21021/r**8)*ilpk(r*k)
    val2 = scint.quad(fee2,tol,qmax/k,epsabs=0.,epsrel=1e-5, limit=100000000)
    return fac*(val1[0]+val2[0])
    
def R(kv, pk, npool = None):

    global ilpk, tol, qmin, qmax
    
    ilpk = pk
    tol = 1.5
    qmin = 1e-5
    qmax = 100.
    
    Rv = parmap.map(R_external, kv.tolist(), pm_processes=npool, pm_chunksize=1, pm_pbar=True)
    toret = numpy.array([kv,Rv])

    del ilpk, tol, qmin, qmax

    return toret
    
'''T kernels'''

def T0( r, x):
    y = 1 + r**2 - 2*r*x
    return 3./16.*r**2*(1.-x**2)

def T1( r, x):
    y = 1 + r**2 - 2*r*x
    return 1./4.*r**2*(1.-x**4)

def T2( r, x):
    y = 1 + r**2 - 2*r*x
    return 1./16.*r**2*(1.+6.*x**2+x**4)

dictT = {1:T0, 2:T1, 3:T2}


#####Define double integral here
def T_internal(k, n, r):
    func = lambda r, x: ilpk(k*numpy.sqrt(1 + r**2 - 2*r*x))*dictT[n](r, x)
    return (glwval*func(r, glxval)).sum(axis = -1)

def T_external(n, k):
    fac = k**3 /(2*numpy.pi)**2	
    r = (kint/k)
    y = T_internal(k,n,r.reshape(-1,1))
    y *= alint**2*pkhint
    return fac*trapz(y, r)
    

def T(kv, pk, al, npool = None, ns = None, kintv = None):

    global ilpk, ilal, kint, pkhint, alint, glxval, glwval

    ilpk = pk
    ilal = al
    if kintv is None:
        kint = numpy.logspace(-5, 3, 2e3)
    else:
        kint = kintv
    pkhint = fc.pgamma(kint)*fc.wg(kint*par.rtens)**2
    alint = ilal(kint)

    glxval, glwval = numpy.array(gl_128_X), numpy.array(gl_128_W)
    glxval = glxval.reshape(1, -1)
    glwval = glwval.reshape(1, -1)

    if ns is None:
        ns = [1, 2, 3]

    prod = product(ns, kv)
    Tv = parmap.starmap(T_external, list(prod), pm_processes=npool, pm_chunksize=1, pm_pbar=True)
    Tv = numpy.array(Tv).reshape(len(ns), -1)
    toret = numpy.zeros([Tv.shape[0] + 1, Tv.shape[1]])
    toret[0] = kv
    toret[1:, :] = Tv

    del ilpk, ilal, kint, pkhint, alint, glxval, glwval

    return toret

'''B kernels'''

def B0( r, x):
    y = 1 + r**2 - 2*r*x
    return 2*x*(1.+r)*(x**2-1)**2
    
def B1( r, x):
    return r**2/2.*(1-x**2)


#####Define double integral here
def B_internal0(k, r):
    func = lambda r, x: fc.pzeta(k*numpy.sqrt(1 + r**2 - 2*r*x))*iltr(k*numpy.sqrt(1 + r**2 - 2*r*x))*B0(r, x)
    return (glwval*func(r, glxval)).sum(axis = -1)

def B_internal1(k, r):
    func = lambda r, x: iltr(k*numpy.sqrt(1 + r**2 - 2*r*x))*B1(r, x)
    return (glwval*func(r, glxval)).sum(axis = -1)

def B_external0( k):
    fac = k**3 /(2*numpy.pi)**2	
    r = (kint/k)
    absr1 = abs(r-1)
    mask = absr1 < tol
    y = numpy.zeros_like(r)
    y[~mask]  = B_internal0(k, r[~mask].reshape(-1, 1))
    if mask.sum():
        y[mask] = B_internal0(k, r[mask].reshape(-1, 1)) #as it is, I'm not masking anything
    y *= alint
    return fac*trapz(y, r)
    
def B_external1( k):
    fac = k**3 /(2*numpy.pi)**2	
    r = (kint/k)
    mask = r < 10.
    y = numpy.zeros_like(r)
    y[~mask]  = B_internal1(k, r[~mask].reshape(-1, 1))
    #if mask.sum():
    #    y[mask] = B_internal(k, r[mask].reshape(-1, 1)) #as it is, I'm not masking anything
    y *= fc.pzeta(kint)*iltr(kint)
    return fac*trapz(y, r)
    
dictB = {1:B_external0, 2:B_external1}

def B_external(n, k):
    return dictB[n](k)

def B(kv, tr, al, npool = None, ns = None, kintv = None):

    global iltr, ilal, kint, alint, glxval, glwval, tol

    iltr = tr
    ilal = al
    if kintv is None:
        kint = numpy.logspace(-5, 3, 2e3)
    else:
        kint = kintv
    alint = ilal(kint)*fc.wg(kint*par.rtens)
    tol = 1e-10
    glxval, glwval = numpy.array(gl_128_X), numpy.array(gl_128_W)
    glxval = glxval.reshape(1, -1)
    glwval = glwval.reshape(1, -1)
    
    if ns is None:
        ns = [1, 2]

    prod = product(ns, kv)
    Bv = parmap.starmap(B_external, list(prod), pm_processes=npool, pm_chunksize=1, pm_pbar=True)
    Bv = numpy.array(Bv).reshape(len(ns), -1)
    toret = numpy.zeros([Bv.shape[0] + 1, Bv.shape[1]])
    toret[0] = kv
    toret[1:, :] = Bv

    del iltr, ilal, kint, alint, glxval, glwval, tol

    return toret


gl_128_X = [9.99825e-01, 9.99077e-01, 9.97733e-01, 9.95793e-01, 9.93257e-01, 9.90128e-01, 9.86407e-01, 9.82096e-01, 9.77198e-01, 9.71717e-01, 9.65654e-01, 9.59015e-01, 9.51802e-01, 9.44020e-01, 9.35674e-01, 9.26769e-01, 9.17310e-01, 9.07303e-01, 8.96753e-01, 8.85668e-01, 8.74053e-01, 8.61915e-01, 8.49263e-01, 8.36103e-01, 8.22443e-01, 8.08292e-01, 7.93657e-01, 7.78548e-01, 7.62974e-01, 7.46944e-01, 7.30468e-01, 7.13554e-01, 6.96215e-01, 6.78459e-01, 6.60298e-01, 6.41742e-01, 6.22802e-01, 6.03490e-01, 5.83818e-01, 5.63797e-01, 5.43438e-01, 5.22755e-01, 5.01760e-01, 4.80464e-01, 4.58881e-01, 4.37025e-01, 4.14906e-01, 3.92540e-01, 3.69940e-01, 3.47118e-01, 3.24088e-01, 3.00865e-01, 2.77463e-01, 2.53894e-01, 2.30174e-01, 2.06316e-01, 1.82334e-01, 1.58244e-01, 1.34059e-01, 1.09794e-01, 8.54636e-02, 6.10820e-02, 3.66638e-02, 1.22237e-02, -9.99825e-01, -9.99077e-01, -9.97733e-01, -9.95793e-01, -9.93257e-01, -9.90128e-01, -9.86407e-01, -9.82096e-01, -9.77198e-01, -9.71717e-01, -9.65654e-01, -9.59015e-01, -9.51802e-01, -9.44020e-01, -9.35674e-01, -9.26769e-01, -9.17310e-01, -9.07303e-01, -8.96753e-01, -8.85668e-01, -8.74053e-01, -8.61915e-01, -8.49263e-01, -8.36103e-01, -8.22443e-01, -8.08292e-01, -7.93657e-01, -7.78548e-01, -7.62974e-01, -7.46944e-01, -7.30468e-01, -7.13554e-01, -6.96215e-01, -6.78459e-01, -6.60298e-01, -6.41742e-01, -6.22802e-01, -6.03490e-01, -5.83818e-01, -5.63797e-01, -5.43438e-01, -5.22755e-01, -5.01760e-01, -4.80464e-01, -4.58881e-01, -4.37025e-01, -4.14906e-01, -3.92540e-01, -3.69940e-01, -3.47118e-01, -3.24088e-01, -3.00865e-01, -2.77463e-01, -2.53894e-01, -2.30174e-01, -2.06316e-01, -1.82334e-01, -1.58244e-01, -1.34059e-01, -1.09794e-01, -8.54636e-02, -6.10820e-02, -3.66638e-02, -1.22237e-02]


gl_128_W = [4.49381e-04, 1.04581e-03, 1.64250e-03, 2.23829e-03, 2.83275e-03, 3.42553e-03, 4.01625e-03, 4.60458e-03, 5.19016e-03, 5.77264e-03, 6.35166e-03, 6.92689e-03, 7.49798e-03, 8.06459e-03, 8.62638e-03, 9.18301e-03, 9.73415e-03, 1.02795e-02, 1.08187e-02, 1.13514e-02, 1.18773e-02, 1.23961e-02, 1.29076e-02, 1.34113e-02, 1.39070e-02, 1.43943e-02, 1.48731e-02, 1.53430e-02, 1.58037e-02, 1.62550e-02, 1.66966e-02, 1.71281e-02, 1.75495e-02, 1.79603e-02, 1.83604e-02, 1.87496e-02, 1.91275e-02, 1.94940e-02, 1.98489e-02, 2.01919e-02, 2.05228e-02, 2.08414e-02, 2.11476e-02, 2.14412e-02, 2.17219e-02, 2.19897e-02, 2.22443e-02, 2.24857e-02, 2.27135e-02, 2.29278e-02, 2.31284e-02, 2.33152e-02, 2.34881e-02, 2.36469e-02, 2.37916e-02, 2.39220e-02, 2.40382e-02, 2.41400e-02, 2.42273e-02, 2.43002e-02, 2.43586e-02, 2.44024e-02, 2.44316e-02, 2.44462e-02, 4.49381e-04, 1.04581e-03, 1.64250e-03, 2.23829e-03, 2.83275e-03, 3.42553e-03, 4.01625e-03, 4.60458e-03, 5.19016e-03, 5.77264e-03, 6.35166e-03, 6.92689e-03, 7.49798e-03, 8.06459e-03, 8.62638e-03, 9.18301e-03, 9.73415e-03, 1.02795e-02, 1.08187e-02, 1.13514e-02, 1.18773e-02, 1.23961e-02, 1.29076e-02, 1.34113e-02, 1.39070e-02, 1.43943e-02, 1.48731e-02, 1.53430e-02, 1.58037e-02, 1.62550e-02, 1.66966e-02, 1.71281e-02, 1.75495e-02, 1.79603e-02, 1.83604e-02, 1.87496e-02, 1.91275e-02, 1.94940e-02, 1.98489e-02, 2.01919e-02, 2.05228e-02, 2.08414e-02, 2.11476e-02, 2.14412e-02, 2.17219e-02, 2.19897e-02, 2.22443e-02, 2.24857e-02, 2.27135e-02, 2.29278e-02, 2.31284e-02, 2.33152e-02, 2.34881e-02, 2.36469e-02, 2.37916e-02, 2.39220e-02, 2.40382e-02, 2.41400e-02, 2.42273e-02, 2.43002e-02, 2.43586e-02, 2.44024e-02, 2.44316e-02, 2.44462e-02]
