import numpy
import user
import param
import power
import bisp
import trisp
   
############
if __name__=="__main__":
    
   args = user.parse_arguments()

   '''Check that input data is available'''
   if args.pfile == None:
       print('Need an input power spectrum')
   elif args.afile == None:
       print('Need an alpha function')
   else:
       s = args.afile
       fmtpos = s.rfind('.')
       args.afile = s[:fmtpos] + '_z%.2f'%(args.z) + s[fmtpos:]
       
       s = args.outfile 
       fmtpos = s.rfind('.')
      
       ''' All contributions to galaxy shape power spectrum from scalar-induced tidal field'''
       if args.ps:
           if args.approx:
                outfilep = 'Output/' + s[:fmtpos] + '_cl_ps_approx_z%.2f'%(args.z) + s[fmtpos:]
           else:
                outfilep = 'Output/' + s[:fmtpos] + '_cl_ps_z%.2f'%(args.z) + s[fmtpos:]
           print('Power Spectrum scalar contribution will be saved at \n%s'%outfilep)
           print('Computing PS scalar contributions now...\n')
           ps = power.PS_cl(pfile = args.pfile, nl = args.nl, kmin = args.kmin, kmax = args.kmax,  npool = args.npool, z = args.z, afile = args.afile, outfile=outfilep);
           print('Saving PS scalar now...\n')
           power.save_pspectra(ps,outfilep,args.nl)
       if args.bs:
           outfileb = 'Output/' + s[:fmtpos] + '_cl_bs_z%.2f'%(args.z) + s[fmtpos:]
           print('Bispectrum scalar contribution will be saved at \n%s'%outfileb)
           print('Computing BS scalar contributions now...\n')
           bs = bisp.BS_cl(pfile = args.pfile, sfile = args.sfile, rfile = args.rfile, nl = args.nl, kmin = args.kmin, kmax = args.kmax,  npool = args.npool, z = args.z, afile = args.afile, outfile=outfileb);
           if args.saver:
                rfile = 'Input/' + s[:fmtpos] + '_Rfuncs' +s[fmtpos:]
                bisp.save_rkernel(bs,rfile)
                print('Saving R kernels at \n%s' %rfile) 
           if args.saves:
                sfile = 'Input/' + s[:fmtpos] + '_Sfuncs' +s[fmtpos:]
                bisp.save_skernel(bs,sfile)
                print('Saving S kernels at \n%s' %sfile)           
           print('Saving BS scalar now...\n')
           bisp.save_bsspectra(bs,outfileb,args.nl)
       if args.ts:
           outfilet = 'Output/' + s[:fmtpos] + '_cl_ts_z%.2f'%(args.z) + s[fmtpos:]
           print('Computing TS scalar contributions now...\n') 
           ts = trisp.TS_cl(pfile = args.pfile, sfile = args.sfile, nl = args.nl, kmin = args.kmin, kmax = args.kmax,  npool = args.npool, z = args.z, afile = args.afile, outfile=outfilet);
           if args.saves:
                sfile = 'Input/' + s[:fmtpos] + '_Sfuncs' +s[fmtpos:]
                trisp.save_skernel(ts,sfile)
                print('Saving S kernels at \n%s' %sfile)
           print('Trispectrum scalar contribution will be saved at \n%s'%outfilet)
           print('Saving TS scalar now...\n')
           trisp.save_tspectra(ts,outfilet,args.nl)
       ''' All contributions to galaxy shape power spectrum from tensor-induced tidal field'''           
       if args.pt:
           if args.approx:
                outfilep = 'Output/' + s[:fmtpos] + '_cl_pt_approx_z%.2f'%(args.z) + s[fmtpos:]
           else:
                outfilep = 'Output/' + s[:fmtpos] + '_cl_pt_z%.2f'%(args.z) + s[fmtpos:]
           print('Power Spectrum tensor contribution will be saved at \n%s'%outfilep)
           print('Computing PS tensor contributions now...\n')
           ps = power.PT_cl(pfile = args.pfile, nl = args.nl, kmin = args.kmin, kmax = args.kmax,  npool = args.npool, z = args.z, afile = args.afile, outfile=outfilep);
           print('Saving PS tensor now...\n')
           power.save_ptspectra(ps,outfilep,args.nl)
       if args.bt:
           outfileb = 'Output/' + s[:fmtpos] + '_cl_bt_z%.2f'%(args.z) + s[fmtpos:]
           print('Bispectrum tensor contribution will be saved at \n%s'%outfileb)
           print('Computing BS tensor contributions now...\n')
           bs = bisp.BT_cl(pfile = args.pfile, bfile = args.bfile, nl = args.nl, kmin = args.kmin, kmax = args.kmax,  npool = args.npool, z = args.z, afile = args.afile, outfile=outfileb);
           if args.saveb:
                bfile = 'Input/' + s[:fmtpos] + '_Bfuncs_z%.2f'%(args.z) +s[fmtpos:]
                bisp.save_bkernel(bs,bfile)
                print('Saving B kernels at \n%s' %bfile) 
           print('Saving BS tensor now...\n')
           bisp.save_btspectra(bs,outfileb,args.nl)
       if args.tt:
           outfilet = 'Output/' + s[:fmtpos] + '_cl_tt_z%.2f'%(args.z) + s[fmtpos:]
           print('Trispectrum scalar contribution will be saved at \n%s'%outfilet)
           print('Computing TS scalar contributions now...\n')
           ts = trisp.TT_cl(pfile = args.pfile, tfile = args.tfile, nl = args.nl, kmin = args.kmin, kmax = args.kmax,  npool = args.npool, z = args.z, afile = args.afile, outfile=outfilet);
           if args.savet:
                tfile = 'Input/' + s[:fmtpos] + '_Tfuncs' +s[fmtpos:]
                trisp.save_tkernel(ts,tfile)
                print('Saving T kernels at \n%s' %tfile)
           print('Saving TS tensor now...\n')
           trisp.save_ttspectra(ts,outfilet,args.nl)
           
