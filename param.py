import numpy

#COSMOLOGICAL PARAMETERS

#cosmology

h = 0.7
H0 = 100./300000.
aeq = 1/(1+3234)
Ol = 0.7
Om = 0.3/(1.+aeq)
Or = Om*aeq
rhoc = 27.75e10*Om

#intrinsic alignment amplitude

tC = 0.12

#primordial parameters

r = 0.1
As = 2.3178e-9
ns = 0.967
kp = 0.002/h
nt = 0.0

chi = 1		#chirality

#late time parameters

cK = 1.0
cdK = 1.0
cd = 1.0
ct = 1.0		
rtens = 1.0		#scale at which we smooth tensor perturbation to avoid they affect galaxy IA at small scales
