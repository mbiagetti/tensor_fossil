import param as par
import fclaux as fc
import kernels as ksp
import user
import tqdm
import parmap
import numpy
import scipy.integrate as scint
from scipy.special import spherical_jn as jn
import scipy.interpolate
from scipy.interpolate import InterpolatedUnivariateSpline as interpolate
from itertools import product

class BS_cl:

    def __init__(self, k = None, p = None, kl = None, al = None, pfile = None, sfile =  None, rfile = None, nl = None, kmin = None, kmax = None,  npool = None, z = None, afile = None, outfile=None):

        self.args = user.parse_arguments()
        k,p = numpy.loadtxt(pfile, unpack=True)
        self.k = k
        self.p = p
        self.tr = interpolate(self.k,numpy.sqrt(self.p/(fc.pzeta(self.k))))
        self.pr = interpolate(self.k,self.p)
        self.pwr = interpolate(self.k,self.p*fc.wg(self.k*par.rtens)**2)
        self.npool = npool
        self.z = z
        self.nl = nl
        self.eta = fc.eta(0.)-fc.eta(self.z)
        self.kmin = kmin
        self.kmax = kmax
        self.outfile = outfile
        if sfile is None:
            self.kq, self.S0, self.S1, self.S2, self.S3 = self.calc_S()
        else:
            self.kq, self.S0, self.S1, self.S2, self.S3  = numpy.loadtxt(sfile, unpack=True)
        self.ilS0 = interpolate(self.kq, self.S0)  
        self.ilS1 = interpolate(self.kq, self.S1)
        self.ilS2 = interpolate(self.kq, self.S2)
        self.ilS3 = interpolate(self.kq, self.S3)
        if rfile is None:
            self.kq, self.R0 = self.calc_R()
        else:
            self.kq, self.R0  = numpy.loadtxt(rfile, unpack=True)
        self.ilR0 = interpolate(self.kq, self.R0)
        self.bEEs = self.bispectraEE_scalar()
    
    '''Evaluation of kernels'''
    
    def calc_S(self):
        print('Evaluating S integrals. Recommend saving them')
        k = numpy.logspace(numpy.log10(self.kmin),2., 2e3)
        ns = [1, 2, 3, 4]
        Sv = ksp.S(kv = k, pk = self.pwr, npool = self.npool, ns = ns, kintv = None)
        return Sv[0], Sv[1], Sv[2], Sv[3], Sv[4]

    def calc_R(self):
        print('Evaluating R integrals. Recommend saving them')
        #k = numpy.logspace(self.lkmin, self.lkmax, 2e3)
        k = numpy.logspace(numpy.log10(self.kmin),1., 2e3)
        ns = [1]
        Rv = ksp.R(kv = k, pk = self.pwr, npool = self.npool)
        return Rv[0], Rv[1]
        
    '''EE scalar-induced bispectrum from perturbation theory'''
        
    def I_externalEE_bisp(self, z, l):
        fac = l * (l+1) / (2*numpy.pi)**2 / (l+2) / (l+1) / l / (l-1)
        fac *= par.cdK**2 * par.cd * fc.Dgrow( self.z)**4 
        fee = lambda k,z,l : k**2*(fc.QES(k*self.eta,l)**2 * (self.ilS3(k) + self.ilR0(k)*self.pwr(k)) )
        val = scint.quad(fee,self.kmin,self.kmax,epsabs=0.,epsrel=1e-7, limit=100000000,  args=(z,l,))
        print('BS EE scalars at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return val[0]*fac
        
    def bispectraEE_scalar(self):
        zi = [ self.z]
        li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        Iebd = parmap.starmap(self.I_externalEE_bisp, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        Iebd = numpy.array(Iebd).reshape(len(zi), -1)
        print("Done EE scalar bispectrum.")
        return Iebd[0]

class BT_cl:

    def __init__(self, k = None, p = None, kl = None, al = None, pfile = None, bfile = None, nl = None, kmin = None, kmax = None,  npool = None, z = None, afile = None, outfile=None):
    
        self.args = user.parse_arguments()
        k,p = numpy.loadtxt(pfile, unpack=True)
        self.k = k
        self.p = p
        self.tr = interpolate(self.k,numpy.sqrt(self.p/(fc.pzeta(self.k))))  #this definition includes the k^2
        ka,al = numpy.loadtxt(afile, unpack=True)
        self.alpha = interpolate(ka,al)
        self.pwr = interpolate(self.k,self.p*fc.wt(self.k*fc.rad(par.mgal))**2)
        self.twr = interpolate(self.k,numpy.sqrt(self.p/(fc.pzeta(self.k)))*fc.wg(self.k*par.rtens))
        self.npool = npool
        self.z = z
        self.nl = nl
        self.eta = fc.eta(0.)-fc.eta(self.z)
        self.kmin = kmin
        self.kmax = kmax
        self.outfile = outfile
        if bfile is None:
            self.kq, self.B0, self.B1 = self.calc_B()
        else:
            self.kq, self.B0, self.B1  = numpy.loadtxt(bfile, unpack=True)
        self.ilB0 = interpolate(self.kq, self.B0)  
        self.ilB1 = interpolate(self.kq, self.B1) 
        self.bEE = self.bispectraEE()
        self.bBB = self.bispectraBB() 
        self.bEB = self.bispectraEB()
        
    '''Evaluation of kernels'''
    
    def calc_B(self):
        print('Evaluating B integrals. Recommend saving them')
        k = numpy.logspace(numpy.log10(self.kmin),numpy.log10(self.kmax), 2e3)
        ns = [1, 2]
        Bv = ksp.B(kv = k, tr = self.twr, al = self.alpha, npool = self.npool, ns = ns, kintv = None)
        return Bv[0], Bv[1], Bv[2]
        
    '''Contribution to EE power spectrum from TSS non-Gaussianity in the squeezed limit'''
        
    def I_externalEE(self, z, l):
        fac = l * (l+1) / (2*numpy.pi)**2
        fac *= par.ct * par.cd * par.cK * fc.Dgrow( self.z)**2
        fee = lambda k,z,l : k**2*fc.QE(k*self.eta,l)*fc.QE(k*self.eta,l)*self.ilB1(k)*fc.pgamma(k)*self.alpha(k)*fc.wg(k*par.rtens)
        val = scint.quad(fee,self.kmin,self.kmax,epsabs=0.,epsrel=1e-7, limit=100000000,  args=(z,l,))
        print('BS EE tensors at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return fac*(val[0])
        
    def bispectraEE(self):
        zi = [ self.z]
        li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        Iebd = parmap.starmap(self.I_externalEE, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        Iebd = numpy.array(Iebd).reshape(len(zi), -1)
        print("Done EE bispectrum.")
        return Iebd[0]
        
    '''Contribution to BB power spectrum from TSS non-Gaussianity in the squeezed limit'''
        
    def I_externalBB(self, z, l):
        fac = l * (l+1) / (2*numpy.pi)**2
        fac *= par.ct * par.cd * par.cK * fc.Dgrow( self.z)**2
        fee = lambda k,z,l : k**2*fc.QB(k*self.eta,l)*fc.QB(k*self.eta,l)*self.ilB1(k)*fc.pgamma(k)*self.alpha(k)*fc.wg(k*par.rtens)
        val = scint.quad(fee,self.kmin,self.kmax,epsabs=0.,epsrel=1e-7, limit=100000000,  args=(z,l,))
        print('BS BB tensors at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return fac*(val[0])
        
    def bispectraBB(self):
        zi = [ self.z]
        li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        Iebd = parmap.starmap(self.I_externalBB, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        Iebd = numpy.array(Iebd).reshape(len(zi), -1)
        print("Done BB bispectrum.")
        return Iebd[0]
        
    '''Contribution to EB power spectrum from TTS chiral non-Gaussianity'''
        
    def I_externalEB(self, z, l):
        fac = l * (l+1) / (2*numpy.pi)**2 * numpy.sqrt(2)
        fac *= par.ct**2 * par.cd * par.pi * par.r * fc.Dgrow( self.z)
        fee = lambda k,z,l : k**2*fc.QB(k*self.eta,l)*fc.QE(k*self.eta,l)*self.ilB0(k)*fc.pgamma(k)*self.alpha(k)*fc.wg(k*par.rtens)
        val = scint.quad(fee,self.kmin,self.kmax,epsabs=0.,epsrel=1e-7, limit=100000000,  args=(z,l,))
        print('BS EB tensors at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return fac*(val[0])

    def bispectraEB(self):
        zi = [ self.z]
        li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        Iebd = parmap.starmap(self.I_externalEB, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        Iebd = numpy.array(Iebd).reshape(len(zi), -1)
        print("Done EB bispectrum.")
        return Iebd[0]

'''Save stuff on file'''
def save_btspectra(bs,outfile,nl):
    header = 'l   EBt   EEt   BBt\n'
    numpy.savetxt(outfile, numpy.array([ numpy.arange(2,nl+1), bs.bEB, bs.bEE, bs.bBB]).T, fmt=['%d','%0.4e','%0.4e','%0.4e'], header=header)
    
def save_bsspectra(bs,outfile,nl):
    header = 'l   EEs\n'
    numpy.savetxt(outfile, numpy.array([ numpy.arange(2,nl+1), bs.bEEs]).T, fmt=['%d','%0.4e'], header=header)

def save_bkernel(cl, bfile):
    '''Save B functions into the file 'bfile'
    '''
    header = 'k[h/Mpc]  B0   B1 \n'
    numpy.savetxt(bfile, numpy.array([cl.kq, cl.B0, cl.B1]).T, fmt='%0.6e', header=header)

def save_rkernel(cl, rfile):
    '''Save R functions into the file 'rfile'
    '''
    header = 'k[h/Mpc]  R0  \n'
    numpy.savetxt(rfile, numpy.array([cl.kq, cl.R0]).T, fmt='%0.6e', header=header)
 
def save_skernel(cl, sfile):
    '''Save S functions into the file 'sfile'
    '''
    header = 'k[h/Mpc]  S0   S1   S2    S3 \n'
    numpy.savetxt(sfile, numpy.array([cl.kq, cl.S0, cl.S1, cl.S2, cl.S3]).T, fmt='%0.6e', header=header)
  
