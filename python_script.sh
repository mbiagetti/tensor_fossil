#!/bin/sh

IN=Input/powL2000_gaussian_085.dat
OUT=ia.dat
AFILE=Input/alpha.dat
SFILE=Input/ia_Sfuncs.dat
RFILE=Input/ia_Rfuncs.dat
TFILE=Input/ia_Tfuncs.dat
BFILE=Input/ia_Bfuncs.dat
NL=100
NP=4
KMAX=10
KMIN=1e-5
RED=2.0

start=$(date)
echo "start time : $start"
                                                                                                                                
python main.py --pfile $IN --afile $AFILE --nl $NL --npool $NP --pt TRUE --kmax $KMAX --z $RED --approx TRUE --rfile $RFILE --sfile $SFILE --saveb TRUE #--bfile $BFILE #--saveb TRUE --outfile $OUT #--sfile $SFILE --rfile $RFILE  #--sfile $SFILE #--z $RED
