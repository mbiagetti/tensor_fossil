#!/bin/sh

# THE NAME OF THE SBATCH JOB

#SBATCH -J tensor_fossil

# REDIRECT OUTPUT TO THE FOLLOWING STREAM
#SBATCH -e RUNinfo/tensor_fossil-error%j
#SBATCH -o RUNinfo/tensor_fossil-out%j

# NUMBER OF NODES
#SBATCH -n 1
#SBATCH --mincpus=24

# ENVIRONMENT
#SBATCH --partition=short
##SBATCH --constraint=[island1|island2|island3|island4|island5] 

# SET THE TIME LIMIT [HRS:MINS:SECS]
#SBATCH --time=01:00:00

#SBATCH --mail-user=m.biagetti@uva.nl
#SBATCH --mail-type=ALL

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/Codes/Libraries
module load 2019
module load Python/3.6.6-intel-2018b
                                                                                                                            
IN=Input/powL2000_gaussian_085.dat
OUT=ia.dat
AFILE=Input/alpha_z2.00.dat
SFILE=Input/ia_Sfuncs.dat
RFILE=Input/ia_Rfuncs.dat
NL=100
NP=24
KMAX=100
KMIN=1e-5
RED=2.0

start=$(date)
echo "start time : $start"
                                                                                                                                
python main.py --pfile $IN --afile $AFILE --nl $NL --npool $NP --pt TRUE --kmax $KMAX --outfile $OUT --z $RED #--limber TRUE  #--saves TRUE --rfile $RFILE #--sfile $SFILE --rfile $RFILE 


