# Primordial Gravitational Waves from Galaxy Intrinsic Alignments

This code implements the calculation of the galaxy shape power spectrum as sourced
by scalar and tensor perturbations, as described in:

Primordial Gravitational Waves from Galaxy Intrinsic Aligments [https://arxiv.org/abs/2001.05930]

Authors: Matteo Biagetti and Giorgio Orlando

## Installation and Running

The code is written in Python 3. It requires the following python packages: 

- `numpy`
- `tdqm`
- `parmap`
- `scipy`
- `itertools`

Two files are required for the code to run: a file containing the linear matter power spectrum
and a file containing the transfer function &alpha;(k<sub>L</sub>,&eta;) (see Section 3.2) as a function of k.
These files should be put in the `Input/` folder.

All options can be found in the file `user.py`. For instance, in order to calculate all 
the tensor-induced contributions to the galaxy shape power spectrum up to l=100 on 4 processors you can type

> python main.py --pfile Input/powL2000_gaussian_085.dat --afile Input/alpha.dat --pt TRUE --bt TRUE --tt TRUE -nl 100 -npool 4

A more detailed example script is available, named `python_script.sh`. A similar script adapted to run on a supercomputer
using the SLURM environment is called `python_script_cartesius.sh`.

## Code Structure

### Overview

The code computes all contributions to the density-weighted galaxy shape power spectrum 
within the Linear Alignment (LA) model. 

These contributions are divided into 2x3 categories:
we distinguish scalar-induced contributions, i.e. contributions sourced by scalar perturbations
(see Section 2), and tensor-induced contributions, i.e. contributions sourced by primordial
gravitational waves (see Sections 3,4 and 5). 

Each of these is divided into 3 subgroups, determined by the number of fields that 
make up the correlator and are outlined in summarising equations Eq. 2.24 for 
scalar-induced contributions and Eq. 5.1 (first, second, fourth and sixth term)
for tensor-induced ones.

All these contributions can be computed independently, you just activate them as indicated 
in the `user.py' file:

- ``--ps TRUE`` activates scalar-induced contribution from two-fields correlator
- ``--pt TRUE`` activates tensor-induced contribution from two-fields correlator
- ``--bs TRUE`` activates scalar-induced contribution from three-fields correlator
- ``--bt TRUE`` activates tensor-induced contribution from three-fields correlator
- ``--ts TRUE`` activates scalar-induced contribution from four-fields correlator
- ``--tt TRUE`` activates tensor-induced contribution from four-fields correlator

All output files are saved in `Output/` folder.
### Components

Here we list all files and explain what they do.

- `main.py`: Checks input data, defines output files and runs all requested contributions.
- `user.py`: Lists all possible options, with a brief explanation.
- `param.py`: Parameter file.
- `fclaux.py`: Generic functions that are used across the code.
- `power.py`: Computes all contributions from two-fields correlators and saves outputs. It is composed of two classes, corresponding to scalar-induced and tensor-induced contributions.
- `bisp.py`: Computes all contributions from three-fields correlators and saves outputs. It is composed of two classes, corresponding to scalar-induced and tensor-induced contributions.
- `trisp.py`: Computes all contributions from four-fields correlators and saves outputs. It is composed of two classes, corresponding to scalar-induced and tensor-induced contributions.
- `kernels.py`: Computes various kernels for the fast computation of multi-dimentional integrals. Kernels can be saved on file upon using the appropriate option in the `user.py` file.
- `python_script.sh`: Script for running the code on a laptop.
- `python_script_cartesius.sh`: Script for running the code on a SLURM environment.






