import param as par
import fclaux as fc
import user
import kernels as ksp
import tqdm
import parmap
import numpy
import scipy.integrate as scint
from scipy.special import spherical_jn as jn
import scipy.interpolate
from scipy.interpolate import InterpolatedUnivariateSpline as interpolate
from scipy.misc import derivative
from itertools import product

class TS_cl:

    def __init__(self, k = None, p = None, kl = None, al = None, pfile = None, sfile =  None, nl = None, kmin = None, kmax = None,  npool = None, z = None, afile = None, outfile=None):

        self.args = user.parse_arguments()
        k,p = numpy.loadtxt(pfile, unpack=True)
        self.k = k
        self.p = p
        self.tr = interpolate(self.k,numpy.sqrt(self.p/(fc.pzeta(self.k))))
        self.pr = interpolate(self.k,self.p)
        self.pwr = interpolate(self.k,self.p*fc.wg(self.k*par.rtens)**2)
        self.npool = npool
        self.z = z
        self.nl = nl
        self.eta = fc.eta(0.)-fc.eta(self.z)
        self.kmin = kmin
        self.kmax = kmax
        self.outfile = outfile
        if sfile is None:
            self.kq, self.S0, self.S1, self.S2, self.S3 = self.calc_S()
        else:
            self.kq, self.S0, self.S1, self.S2, self.S3  = numpy.loadtxt(sfile, unpack=True)
        self.ilS0 = interpolate(self.kq, self.S0)  
        self.ilS1 = interpolate(self.kq, self.S1)
        self.ilS2 = interpolate(self.kq, self.S2)
        self.ilS3 = interpolate(self.kq, self.S3)
        self.tEEs = self.trispectraEE_scalar()
        self.tBBs = self.trispectraBB_scalar()
        
    def calc_S(self):
        print('Evaluating S integrals. Recommend saving them')
        k = numpy.logspace(numpy.log10(self.kmin),numpy.log10(self.kmax), 2e3)
        ns = [1, 2, 3, 4]
        Sv = ksp.S(kv = k, pk = self.pwr, npool = self.npool, ns = ns, kintv = None)
        return Sv[0], Sv[1], Sv[2], Sv[3], Sv[4]
        
    '''Contribution to EE power spectrum from scalar-induced trispectrum'''
        
    def I_externalEE_scalar(self, z, l):
        fac = l * (l+1) / (2*numpy.pi)**2
        fac *= par.cK**2 * par.cd**2 * fc.Dgrow( self.z)**4
        fee = lambda k,z,l : k**2*(fc.QES(k*self.eta,l)**2 * self.ilS0(k) / (l+2) / (l+1) / l / (l-1)
                             +fc.QES1(k*self.eta,l)**2*self.ilS1(k)/(l**2+l-2.)
                             +fc.QE(k*self.eta,l)**2*self.ilS2(k))*(1.-self.ilS4(k))
        val = scint.quad(fee,self.kmin,self.kmax,epsabs=0.,epsrel=1e-7, limit=100000000,  args=(z,l,))
        print('TS EE scalars at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return fac*(val[0])
        
    def trispectraEE_scalar(self):
        zi = [ self.z]
        li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        if self.args.approx:
             Iebd = parmap.starmap(self.I_externalEE_scalar_approx, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        else:
             Iebd = parmap.starmap(self.I_externalEE_scalar, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        Iebd = numpy.array(Iebd).reshape(len(zi), -1)
        print("Done EE scalar trispectrum.")
        return Iebd[0]
        
    '''Contribution to BB power spectrum from scalar-induced trispectrum'''
        
    def I_externalBB_scalar(self, z, l):
        fac = l * (l+1) / (2*numpy.pi)**2
        fac *= par.cK**2 *par.cd**2 * fc.Dgrow( self.z)**4
        fee = lambda k,z,l : k**2*(fc.QBS1(k*self.eta,l)**2*(self.ilS1(k))/(l**2+l-2.)
                             +fc.QB(k*self.eta,l)**2*(self.ilS2(k)))*(1.-self.ilS4(k))
        val = scint.quad(fee,self.kmin,self.kmax,epsabs=0.,epsrel=1e-5, limit=1000000,  args=(z,l,))
        print('TS BB scalars at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return (val[0]*fac)
                
    def trispectraBB_scalar(self):
        zi = [ self.z]
        li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        if self.args.approx:
             Iebd = parmap.starmap(self.I_externalBB_scalar_approx, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        else:
             Iebd = parmap.starmap(self.I_externalBB_scalar, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        Iebd = numpy.array(Iebd).reshape(len(zi), -1)
        print("Done BB scalar trispectrum.")
        return Iebd[0]
        
class TT_cl:

    def __init__(self, k = None, p = None, kl = None, al = None, pfile = None, tfile = None, nl = None, kmin = None, kmax = None,  npool = None, z = None, afile = None, outfile=None):

        self.args = user.parse_arguments()
        k,p = numpy.loadtxt(pfile, unpack=True)
        self.k = k
        self.p = p
        self.pr = interpolate(k,p)#fc.loginterp(k,p)
        self.pwr = interpolate(self.k,self.p*fc.wt(self.k*fc.rad(par.mgal))**2)
        ka,al = numpy.loadtxt(afile, unpack=True)
        self.ka = ka
        self.al = al
        self.alpha = interpolate(self.ka,self.al)
        self.npool = npool
        self.z = z
        self.nl = nl
        self.eta = fc.eta(0.)-fc.eta(self.z)
        self.kmin = kmin
        self.kmax = kmax
        self.outfile = outfile
        if tfile is None:
            self.kq, self.T0, self.T1, self.T2 = self.calc_T()
        else:
            self.kq, self.T0, self.T1, self.T2  = numpy.loadtxt(tfile, unpack=True)
        self.ilT0 = interpolate(self.kq, self.T0)  
        self.ilT1 = interpolate(self.kq, self.T1)
        self.ilT2 = interpolate(self.kq, self.T2)
        self.tEEt = self.trispectraEE_tensor()
        self.tEBt = self.trispectraEB_tensor()
        self.tBBt = self.trispectraBB_tensor()
        
    '''Evaluation of kernels'''
    def calc_T(self):
        print('Evaluating T integrals. Recommend saving them')
        k = numpy.logspace(numpy.log10(self.kmin),numpy.log10(self.kmax), 2e3)
        ns = [1, 2, 3]
        Tv = ksp.T(kv = k, pk = self.pwr, al = self.alpha, npool = self.npool, ns = ns, kintv = None)
        return Tv[0], Tv[1], Tv[2], Tv[3]
        
    '''Contribution to EE power spectrum from tensor-induced trispectrum'''
    
    def I_externalEE_tensor(self, z, l):
        fac = l * (l+1) / (2*numpy.pi)**2
        fac *= par.cK**2 * par.ct**2 * fc.Dgrow( self.z)**2
        fee = lambda k,z,l : k**2*(fc.QES(k*self.eta,l)**2 * self.ilT0(k) / (l+2) / (l+1) / l / (l-1)
                             +fc.QES1(k*self.eta,l)**2*self.ilT1(k)/(l**2+l-2.)
                             +fc.QE(k*self.eta,l)**2*self.ilT2(k))
        val = scint.quad(fee,self.kmin,self.kmax,epsabs=0.,epsrel=1e-7, limit=100000000,  args=(z,l,))
        print('TS EE scalars at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return fac*(val[0])

    def trispectraEE_tensor(self):
        zi = [ self.z]
        li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        Iebd = parmap.starmap(self.I_externalEE_tensor, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        Iebd = numpy.array(Iebd).reshape(len(zi), -1)
        print("Done EE tensor trispectrum.")
        return Iebd[0]
        
    '''Contribution to EB power spectrum from tensor-induced trispectrum'''        
        
    def I_externalEB_tensor(self, z, l):
        fac = l * (l+1) / (2*numpy.pi)**2
        fac *= par.cK**2 * par.ct**2 * fc.Dgrow( self.z)**2
        fee = lambda k,z,l : k**2*(fc.QES1(k*self.eta,l)*fc.QBS1(k*self.eta,l)*self.ilT1(k)/(l**2+l-2.)
                             +fc.QE(k*self.eta,l)*fc.QB(k*self.eta,l)*self.ilT2(k))
        val = scint.quad(fee,self.kmin,self.kmax,epsabs=0.,epsrel=1e-7, limit=100000000,  args=(z,l,))
        print('TS EB scalars at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return fac*(val[0])

    def trispectraEB_tensor(self):
        zi = [ self.z]
        li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        Iebd = parmap.starmap(self.I_externalEB_tensor, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        Iebd = numpy.array(Iebd).reshape(len(zi), -1)
        print("Done EB tensor trispectrum.")
        return Iebd[0]
            
    '''Contribution to BB power spectrum from tensor-induced trispectrum'''
    
    def I_externalBB_tensor(self, z, l):
        fac = l * (l+1) / (2*numpy.pi)**2
        fac *= par.cK**2 * par.ct**2 * fc.Dgrow( self.z)**2
        fee = lambda k,z,l : k**2*(fc.QBS1(k*self.eta,l)**2*self.ilT1(k)/(l**2+l-2.)
                             +fc.QB(k*self.eta,l)**2*self.ilT2(k))
        val = scint.quad(fee,self.kmin,self.kmax,epsabs=0.,epsrel=1e-7, limit=100000000,  args=(z,l,))
        print('TS BB scalars at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return fac*(val[0])
                     
    def trispectraBB_tensor(self):
        zi = [ self.z]
        li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        Iebd = parmap.starmap(self.I_externalBB_tensor, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        Iebd = numpy.array(Iebd).reshape(len(zi), -1)
        print("Done BB tensor trispectrum.")
        return Iebd[0]

'''Save stuff in file'''
def save_tspectra(ts,outfile,nl):
    header = 'l   EEs       BBs\n'
    numpy.savetxt(outfile, numpy.array([ numpy.arange(2,nl+1), ts.tEEs, ts.tBBs]).T, fmt=['%d','%0.4e','%0.4e'], header=header)

def save_ttspectra(ts,outfile,nl):
    header = 'l   EEt		EBt		BBt\n'
    numpy.savetxt(outfile, numpy.array([ numpy.arange(2,nl+1), ts.tEEt, ts.tEBt, ts.tBBt]).T, fmt=['%d','%0.4e','%0.4e', '%0.4e'], header=header)   

def save_skernel(cl, sfile):
    '''Save S functions into the file 'sfile'
    '''
    header = 'k[h/Mpc]  S0   S1   S2    S3 \n'
    numpy.savetxt(sfile, numpy.array([cl.kq, cl.S0, cl.S1, cl.S2, cl.S3]).T, fmt='%0.6e', header=header)

def save_tkernel(cl, tfile):
    '''Save T functions into the file 'tfile'
    '''
    header = 'k[h/Mpc]  T0   T1   T2 \n'
    numpy.savetxt(tfile, numpy.array([cl.kq, cl.T0, cl.T1, cl.T2]).T, fmt='%0.6e', header=header)
