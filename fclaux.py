import numpy
import param as par
import user
import scipy.integrate as scint
from scipy.special import spherical_jn as jn
from scipy.special import gamma
from scipy.interpolate import InterpolatedUnivariateSpline as interpolate
from scipy.misc import derivative

args = user.parse_arguments()

'''Background cosmology'''
def a( z):
	return 1./(1.+z)
#radiation neglected here
def Dgrow( z):
    hsq = par.Om/a(z)**3+(1.-par.Om-par.Ol)/a(z)**2+par.Ol
    oma = par.Om/(a(z)**3*hsq)
    ola = par.Ol/hsq
    gad = 25./10.*oma/(oma**(4./7.) - ola + (1. + oma/2)*(1. + ola/70.))
    return a(z)*gad
    
def eta( z):
    integ = lambda avec: 1/numpy.sqrt(avec*par.Om+avec**4*par.Ol+par.Or)
    return 1/par.H0*scint.quad(integ,0,1/(1+z))[0]

def etaM( z):
    return 2/par.H0*(numpy.sqrt(a(z)+par.aeq)-numpy.sqrt(par.aeq))

def Hz( z):
    return par.H0*numpy.sqrt(par.Om/a(z)**3+par.Ol+par.Or/a(z)**4)
    
'''Window Functions'''

def wt(x):
#    if x<1:
#        val = 1-(x)**2/10.+(x)**4/280.
#    else:
    val = 3*numpy.sin(x)/(x)**3 - 3*numpy.cos(x)/(x)**2
    return val
    
def dwt(x):
#    if x<1:
#        val = 1-(x)**2/10.+(x)**4/280.
#    else:
    val = 3*(numpy.cos(x)/x**2-3*numpy.sin(x)/(x)**3 +numpy.sin(x)/(x))
    return val

def wg(x):
#    if x<0.1:
#        val = 1-(x)**2+(x)**4/2.
#    else:
    val = numpy.exp(-x**2/2)
    return val
    
def rad( m):
    return (3*m/4./numpy.pi/par.rhoc)**(1./3.);

'''Primordial power spectra'''
#Define primordial power spectrum
def pzeta( k):
     amp = 2*numpy.pi**2*par.As
     return amp/k**3*(k/par.kp)**(par.ns-1)
     
def pgamma( k):
     amp = 2*numpy.pi**2*par.As*par.r
     return amp/k**3*(k/par.kp)**par.nt

'''Transfer Functions from fossil computations'''

#alpha in MD
def alphaM( x):
	return 2./5. + 18.*numpy.cos(x)/x**4+6*numpy.sin(x)/x**3*(1.-3./x**2)

#Re[Q2]
def QE( x, l):
    val =  ( - l**2 - 3*l - 2 ) * jn(l,x)/x**2 + 2 * jn(l+1,x) / x + 2 * jn(l,x)
    return val
#Im[Q2]
def QB( x, l):
    val =  2 * (l + 2) * jn(l,x) / x - 2. * jn(l+1,x)
    return val
#Re[Q1]    
def QES1( x, l):
    val = (-2 + l + l**2) * jn(l,x) / x
    return val
#Im[Q1]
def QBS1( x, l):
    val = (-2 + l + l**2) * ( (l+1) * jn(l,x) / x**2 - jn(l+1,x) / x )
    return val
#Re[Q0]  
def QES( x, l):
    val = ( l - 1.) * l * (l + 1.) * (l + 2.) * jn(l,x) / x**2  
    return val   

	
