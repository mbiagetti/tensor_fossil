import param as par
import fclaux as fc
import user
import tqdm
import parmap
import numpy
import scipy.integrate as scint
from scipy.special import spherical_jn as jn
from scipy.special import jn as Jn
import scipy.interpolate
from scipy.interpolate import InterpolatedUnivariateSpline as interpolate
from itertools import product

class PS_cl:

    def __init__(self, k = None, p = None, kl = None, al = None, pfile = None, nl = None, kmin = None, kmax = None,  npool = None, z = None, afile = None, outfile=None):

        self.args = user.parse_arguments()
        k,p = numpy.loadtxt(pfile, unpack=True)
        self.k = k
        self.p = p
        self.pr = interpolate(self.k,self.p)
        self.tr = interpolate(self.k,numpy.sqrt(self.p/(fc.pzeta(self.k))))  #this definition includes the k^2
        self.npool = npool
        self.z = z
        self.eta = fc.eta(0.)-fc.eta(self.z)
        self.nl = nl
        self.l = [ i for i in range(2, self.nl+1)]
        self.kmin = kmin
        self.kmax = kmax
        self.outfile = outfile
        self.pEEs = self.pspectraEEs(zi = [ self.z], li = self.l)
        
    '''Power spectrum EE from scalar-induced contributions at fixed redshift'''

    def integral_gg_ee_s(self, z, l):
        fac = l * (l+1) / (2.*numpy.pi)**2 
        fac *= (l-1) * l * (l+1) * (l+2) / self.eta**4
        fac *= par.cK**2 * fc.Dgrow( self.z)**2
        fee = lambda k,z,l : self.pr(k) * jn(l, k*self.eta)**2 / k**2 * fc.wg(k*par.rtens)**2
        val = scint.romberg(fee,self.kmin,self.kmax,tol=0.,rtol=1e-5, divmax=30,  args=(z,l,))
        print('PS EE scalars at l=%d is %4e ' %(l,fac*val))
        return val*fac        
        
    '''APPROXIMATION: Power spectrum EE from scalar-induced contributions at fixed redshift'''

    def integral_gg_ee_s_approx(self, z, l):
        fac = l * (l+1) / (2.*numpy.pi)
        fac *= (l-1) * l * (l+1) * (l+2) / 2 / self.eta**6
        fac *= par.cK**2 * fc.Dgrow( self.z)**2
        k0 = (l+0.5) / self.eta
        fee = lambda y,z,l : self.pr(k0/numpy.sin(y)) * fc.wg(k0/numpy.sin(y)*par.rtens)**2 * numpy.sin(y)**2 / k0**3
        ymin = numpy.arcsin(k0/self.kmax)
        ymax = numpy.pi/2.
        val = scint.quad(fee,ymin,ymax, epsabs=0.,epsrel=1e-5, limit = 100000, args=(z,l,))
        print('PS EE scalars approximation at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return val[0]*fac

    '''PARALLELIZED NUMERICAL INTEGRATION FOR POWER SPECTRUM'''

    def pspectraEEs(self, zi = None, li = None):
        if zi is None:
            zi = [ self.z]
        if li is None:
            li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        if self.args.approx:
            Iee = parmap.starmap(self.integral_gg_ee_s_approx, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        else:
            Iee = parmap.starmap(self.integral_gg_ee_s, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)  
        Iee = numpy.array(Iee).reshape(len(zi), -1)
        print("Done EE scalars.")
        return Iee[0]
        
class PT_cl:

    def __init__(self, k = None, p = None, kl = None, al = None, pfile = None, nl = None, kmin = None, kmax = None,  npool = None, z = None, afile = None, outfile=None):
    
        self.args = user.parse_arguments()
        k,p = numpy.loadtxt(pfile, unpack=True)
        self.k = k
        self.p = p
        self.pr = interpolate(self.k,self.p)
        self.tr = interpolate(self.k,numpy.sqrt(self.p/(fc.pzeta(self.k))))  #this definition includes the k^2
        ka,al = numpy.loadtxt(afile, unpack=True)
        self.ka = ka
        self.al = al
        self.alpha = interpolate(self.ka,self.al)
        self.dk,self.dpal = numpy.loadtxt("Input/derivpgammaalpha2.dat", unpack=True)
        self.dpalpha = interpolate(self.dk, self.dpal)
        self.npool = npool
        self.z = z
        self.eta = fc.eta(0.)-fc.eta(self.z)
        self.nl = nl
        self.l = [ i for i in range(2, self.nl+1)]
        self.kmin = kmin
        self.kmax = kmax
        self.outfile = outfile
        self.pEEt = self.pspectraEEt(zi = [ self.z], li = self.l)
        self.pEBt = self.pspectraEBt(zi = [ self.z], li = self.l)
        self.pBBt = self.pspectraBBt(zi = [ self.z], li = self.l)
        
    '''Power spectrum EE from tensor-induced contributions at fixed redshift'''
	
    def integral_gg_ee_t(self, z, l):
        fac = l*(l+1)/(2*numpy.pi)/32./numpy.pi
        fac *= par.cK**2
        fee = lambda k,z,l : k**2*fc.pgamma(k)*fc.QE(k*self.eta,l)**2*self.alpha(k)**2*fc.wg(k*par.rtens)**2
        val = scint.quad(fee,self.kmin,self.kmax,epsabs=0.,epsrel=1e-12, limit=100000,  args=(z,l,))
        print('PS EE tensors at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return val[0]*fac

    '''APPROXIMATION: Power spectrum EE from tensor-induced contributions at fixed redshift'''

    def integrand_gg_ee_t_approx(self, k, z, l):
        k0 = (l+0.5)/self.eta
        i1 = (l**2+l+2.-2.*(k*self.eta)**2)**2
        i1 /= 2.*self.eta**6*k**3*numpy.sqrt(numpy.abs(k**2-k0**2))
        i2 = 2./self.eta**4/k**3*numpy.sqrt(numpy.abs(k**2-k0**2))
        i3 = (l**2+l+2.+2.*(k*self.eta)**2)
        i3 /= self.eta**6*k**3*numpy.sqrt(numpy.abs(k**2-k0**2))
        i3b = (l**2+l+2.-2.*(k*self.eta)**2)
        i3b /= self.eta**6*k**2*numpy.sqrt(numpy.abs(k**2-k0**2))
        result = fc.pgamma(k)*self.alpha(k)**2*fc.wg(k*par.rtens)**2*(i1+i2+i3)
        result -= self.dpalpha(k)*i3b
        return result
        
    def integral_gg_ee_t_approx(self, z, l):
        fac = l*(l+1)/(2*numpy.pi)/32./numpy.pi
        fac *= par.cK**2 
        fee = lambda k,z,l : self.integrand_gg_ee_t_approx(k,z,l)
        val = scint.quad(fee,(l+0.5)/self.eta,self.kmax,epsabs=0.,epsrel=1e-10, limit=10000000,  args=(z,l,))
        print('PS EE tensors approx at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return val[0]*fac
        
        
    '''Power spectrum EB from tensor-induced contributions at fixed redshift'''
	
    def integral_gg_eb_t(self, z, l):
        fac = l*(l+1)/(2*numpy.pi)/32./numpy.pi
        fac *= par.cK**2 
        fee = lambda k,z,l : k**2*fc.pgamma(k)*fc.QE(k*self.eta,l)*fc.QB(k*self.eta,l)*self.alpha(k)**2*fc.wg(k*par.rtens)**2
        val = scint.quad(fee,self.kmin,self.kmax,epsabs=0.,epsrel=1e-12, limit=10000000,  args=(z,l,))
        print('PS EB tensors approx at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return val[0]*fac
        
    '''APPROXIMATION: Power spectrum EB from tensor-induced contributions at fixed redshift'''
        
    def integrand_gg_eb_t_approx(self, k, z, l):
        k0 = (l+0.5)/self.eta
        i1 = (l**2+l+2.-2.*(k*self.eta)**2)
        i1 *= 2/self.eta**5/(k**2*numpy.sqrt(numpy.abs(k**2-k0**2)))
        i2 = 2./self.eta**3/k**2*numpy.sqrt(numpy.abs(k**2-k0**2))
        i3 = 2./self.eta**3/numpy.sqrt(numpy.abs(k**2-k0**2))
        i3b = (l**2+l+6.-2.*(k*self.eta)**2)
        i3b /= 2*self.eta**5*k*numpy.sqrt(numpy.abs(k**2-k0**2))
        result = fc.pgamma(k)*self.alpha(k)**2*fc.wg(k*par.rtens)**2*(-i1-i2-i3)
        result += self.dpalpha(k)*i3b
        return result
        
    def integral_gg_eb_t_approx(self, z, l):
        fac = l*(l+1)/(2*numpy.pi)/32./numpy.pi
        fac *= par.cK**2 
        fee = lambda k,z,l : self.integrand_gg_eb_t_approx(k,z,l)
        val = scint.quad(fee,(l+0.5)/self.eta,self.kmax,epsabs=0.,epsrel=1e-10, limit=1000000,  args=(z,l,))
        print('PS EB tensors approx at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return val[0]*fac

    '''Power spectrum BB from tensor-induced contributions at fixed redshift'''

    def integral_gg_bb_t(self, z, l):
        fac = l*(l+1)/(2*numpy.pi)/32./numpy.pi
        fac *= par.cK**2
        fee = lambda k,z,l : k**2*fc.pgamma(k)*fc.QB(k*self.eta,l)*fc.QB(k*self.eta,l)*self.alpha(k)**2*fc.wg(k*par.rtens)**2
        val = scint.quad(fee,self.kmin,self.kmax,epsabs=0.,epsrel=1e-12, limit=10000000,  args=(z,l,))
        print('PS BB tensors at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return val[0]*fac
        
    '''APPROXIMATION Power spectrum BB from tensor-induced contributions at fixed redshift'''
        
    def integrand_gg_bb_t_approx(self, k, z, l):
        k0 = (l+0.5)/self.eta
        i1 = 8./self.eta**4/(k*numpy.sqrt(numpy.abs(k**2-k0**2)))
        i2 = 2./self.eta**2/k*numpy.sqrt(numpy.abs(k**2-k0**2))
        i3 = 4./self.eta**4/(numpy.sqrt(numpy.abs(k**2-k0**2)))
        result = fc.pgamma(k)*self.alpha(k)**2*fc.wg(k*par.rtens)**2*(i1+i2-i3)
        result -= self.dpalpha(k)*i3*k
        return result
        
    def integral_gg_bb_t_approx(self, z, l):
        fac = l*(l+1)/(2*numpy.pi)/32./numpy.pi
        fac *= par.cK**2 
        fee = lambda k,z,l : self.integrand_gg_bb_t_approx(k,z,l)
        val = scint.quad(fee,(l+0.5)/self.eta,self.kmax,epsabs=0.,epsrel=1e-10, limit=10000000,  args=(z,l,))
        print('PS BB tensors approx at l=%d is %4e +- %4e' %(l,fac*val[0],fac*val[1]))
        return val[0]*fac


    '''PARALLELIZED NUMERICAL INTEGRATION FOR POWER SPECTRA'''
        
    def pspectraEEt(self, zi = None, li = None):
        if zi is None:
            zi = [ self.z]
        if li is None:
            li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        if self.args.approx:
            Iee = parmap.starmap(self.integral_gg_ee_t_approx, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        else:
            Iee = parmap.starmap(self.integral_gg_ee_t, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        Iee = numpy.array(Iee).reshape(len(zi), -1)
        print("Done EE tensors.")
        return Iee[0]

    def pspectraEBt(self, zi = None, li = None):
        if zi is None:
            zi = [ self.z]
        if li is None:
            li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        if self.args.approx:
            Ieb = parmap.starmap(self.integral_gg_eb_t_approx, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        else:
            Ieb = parmap.starmap(self.integral_gg_eb_t, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        Ieb = numpy.array(Ieb).reshape(len(zi), -1)
        print("Done EB tensors.")
        return Ieb[0]

    def pspectraBBt(self, zi = None, li = None):
        if zi is None:
            zi = [ self.z]
        if li is None:
            li = [ i for i in range(2, self.nl+1)]
        prod = product(zi, li)
        if self.args.approx:
            Ibb = parmap.starmap(self.integral_gg_bb_t_approx, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        else:
            Ibb = parmap.starmap(self.integral_gg_bb_t, list(prod), pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
        Ibb = numpy.array(Ibb).reshape(len(zi), -1)
        print("Done BB tensors.")
        return Ibb[0]
        
''' Saving everything in files'''
	
def save_ptspectra(ps,outfile,nl):
    header = 'l   EEt      EBt     BBt\n'
    numpy.savetxt(outfile, numpy.array([ numpy.arange(2,nl+1), ps.pEEt, ps.pEBt, ps.pBBt]).T, fmt=['%d', '%0.4e', '%0.4e', '%0.4e'], header=header)
    
def save_pspectra(ps,outfile,nl):
    header = 'l EEs\n'
    numpy.savetxt(outfile, numpy.array([ numpy.arange(2,nl+1), ps.pEEs]).T, fmt=['%d','%0.4e'], header=header)
    
