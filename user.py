import argparse

def parse_arguments():
   parser = argparse.ArgumentParser()
   parser.add_argument('--pfile', required=True, help = 'Input linear matter power spectrum in order - k, p')
   parser.add_argument('--afile', default = None, help='File with table of alphaLCDM at redshift z in order - k, alpha')
   parser.add_argument('--sfile', default = None, help='File with table of S kernels z in order - k, S')
   parser.add_argument('--rfile', default = None, help='File with table of R kernels z in order - k, R')
   parser.add_argument('--tfile', default = None, help='File with table of T kernels z in order - k, T')
   parser.add_argument('--bfile', default = None, help='File with table of B kernels z in order - k, B')
   parser.add_argument('--saves', default = False, help='Save S kernel in a file')   
   parser.add_argument('--saver', default = False, help='Save R kernel in a file')
   parser.add_argument('--savet', default = False, help='Save T kernel in a file')
   parser.add_argument('--saveb', default = False, help='Save B kernel in a file')
   parser.add_argument('--npool', default = 16, type = int, help ="Number of processors")
   parser.add_argument('--nl', default = 50, type = int, help ="l-max to estimate power spectrum at")
   parser.add_argument('--z', default = 2, type = float, help ="Redshift")
   parser.add_argument('--kmin', default = 1e-5, type = float, help ="minimum k in h/Mpc")
   parser.add_argument('--kmax', default = 100, type = float, help ="maximum k in h/Mpc")
   parser.add_argument('--approx', default = False, help = 'Use approximation from Appendix E for integrals in multipole space')
   parser.add_argument('--ps', default = False, help = 'Calculate power spectrum scalar contributions in multipole space')
   parser.add_argument('--bs', default = False, help = 'Calculate bispectrum scalar contributions in multipole space')
   parser.add_argument('--ts', default = False, help = 'Calculate trispectrum scalar contributions in multipole space')
   parser.add_argument('--pt', default = False, help = 'Calculate power spectrum tensor contributions in multipole space')
   parser.add_argument('--bt', default = False, help = 'Calculate bispectrum tensor contributions in multipole space')
   parser.add_argument('--tt', default = False, help = 'Calculate trispectrum tensor contributions in multipole space')
   parser.add_argument('--outfile', default = 'ia.dat', help ="output file stem")

   args = parser.parse_args()

   return args
